import { QDataTable, clone } from 'quasar'
import Vue from 'vue'
import { mapState, mapGetters, mapActions, mapMutations } from 'vuex'

const defaultOptions = [
  { label: '5', value: 5 },
  { label: '10', value: 10 },
  { label: '15', value: 15 },
  { label: '20', value: 20 },
  { label: '50', value: 50 },
  { label: '100', value: 100 }
]

function parseOptions (opts) {
  return opts.map(opt => {
    return {
      label: '' + opt,
      value: Number(opt)
    }
  })
}

const genMixin = (namespace) => {
  return {
    beforeCreate () {
      Vue.delete(this.$options.props, 'data')
    },
    mounted () {
      if (!this.reload) {
        this.loadParams()
      }
    },
    watch: {
      'sorting.dir' (val) {
        this.sendParams()
      },
      filtering: {
        handler (val) {
          this.sendParams()
        },
        deep: true
      },
      pagination: {
        handler (val) {
          this.sendParams()
        },
        deep: true
      }
    },
    computed: {
      data () {
        return this.list
      },
      rows () {
        let length = this.data.length

        if (!length) {
          return []
        }

        let rows = clone(this.data)
        rows.forEach((row, i) => {
          row.__index = i
        })

        this.pagination.entries = this.savedPagination.entries

        return rows
      },
      pagination () {
        console.log('called')
        let self = this, cfg = this.config.pagination, options = defaultOptions

        if (cfg) {
          if (cfg.options) {
            options = parseOptions(cfg.options)
          }
        }

        return {
          get page () { return self.$data._pagination.page },
          set page (page) { self.$data._pagination.page = page },
          get entries () { return self.$data._pagination.entries },
          set entries (entries) { self.$data._pagination.entries = entries },
          get rowsPerPage () {
            let rowsPerPage = self.$data._pagination.rowsPerPage
            if (rowsPerPage == null) {
              if (cfg && typeof cfg.rowsPerPage !== 'undefined') {
                rowsPerPage = cfg.rowsPerPage
              }
              else {
                rowsPerPage = 0
              }
            }
            return rowsPerPage
          },
          set rowsPerPage (rowsPerPage) { self.$data._pagination.rowsPerPage = rowsPerPage },
          options
        }
      },
      ...mapGetters(namespace, ['list', 'isLoading']),
      ...mapState(namespace, { savedPagination: 'pagination', savedFiltering: 'filtering', reload: 'reload' })
    },
    methods: {
      ...mapActions(namespace, ['doQuery']),
      ...mapMutations(namespace, ['setFiltering']),
      loadParams () {
        const { page, entries, rowsPerPage } = this.savedPagination
        this.pagination.page = page
        this.pagination.entries = entries
        this.pagination.rowsPerPage = rowsPerPage

        const { field, terms } = this.savedFiltering
        this.filtering.field = field
        this.filtering.terms = terms
      },
      sendParams () {
        const { sorting, filtering, pagination } = this

        this.setFiltering(filtering)

        this.doQuery({ sorting, filtering, pagination })
      }
    }
  }
}

export default (namespace) => {
  return {
    extends: QDataTable,
    mixins: [genMixin(namespace)]
  }
}
