const files = {
  Blank: [
    'menu.vue',
    'routes.js',
    'store.js',
    'vue'
  ]
}

const actions = (answer) => {
  const copyActions = files['Blank'].map(item => {
    return {
      type: 'add',
      path: `src/components/Page/{{ name }}/{{ name }}.${item}`,
      templateFile: `templates/Page/Blank/Blank.${item}`
    }
  })

  const modifActions =
        [
          {
            type: 'modify',
            path: `src/components/Page/Page.vue`,
            pattern: /(<!-- new tab -->)/,
            template: `<q-route-tab defaults slot="title" icon="{{ icon }}" to="/page/{{ kebabCase name }}" />\n      $1`
          },
          {
            type: 'modify',
            path: `src/components/Page/Page.menu.js`,
            pattern: /(\/\* x-menu \*\/)/,
            template: `,\n  '{{ name }}'$1`
          },
          {
            type: 'modify',
            path: `src/components/Page/Page.routes.js`,
            pattern: /(\/\* x-import-routes \*\/)/,
            template: `$1\nimport {{ name }} from './{{ name }}/{{ name }}.routes'`
          },
          {
            type: 'modify',
            path: `src/components/Page/Page.routes.js`,
            pattern: /(\/\* x-spread-children \*\/)/,
            template: `$1\n  ...{{ name }},`
          },
          {
            type: 'modify',
            path: `src/components/Page/Page.store.js`,
            pattern: /(\/\* x-import-store \*\/)/,
            template: `$1\nimport {{ name }} from './{{ name }}/{{ name }}.store'`
          },
          {
            type: 'modify',
            path: `src/components/Page/Page.store.js`,
            pattern: /(\/\* x-add-module \*\/)/,
            template: `$1\n  {{ name }},`
          }]

  return [...copyActions, ...modifActions]
}

const rootActions = (answer) => {
  const copyActions = ['routes.js', 'vue'].map(item => {
    return {
      type: 'add',
      path: `src/components/{{ name }}/{{ name }}.${item}`,
      templateFile: `templates/Root/Root.${item}`
    }
  })

  const modifActions = [
    {
      type: 'modify',
      path: `src/router.js`,
      pattern: /(\/\* x-import-routes \*\/)/,
      template: `$1\nimport {{ name }} from '@/{{ name }}/{{ name }}.routes'`
    },
    {
      type: 'modify',
      path: `src/router.js`,
      pattern: /(\/\* x-inject-routes \*\/)/,
      template: `$1\n  ...{{ name }}(),`
    }
  ]

  return [...copyActions, ...modifActions]
}

module.exports = (plop) => {
  plop.setGenerator('page', {

    description: 'Create a new page',

    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your page name?'
      },
      {
        type: 'input',
        name: 'icon',
        message: 'What is icon name?'
      }

    ],
    actions
  })

  plop.setGenerator('root', {

    description: 'Create a new root page',

    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your root page?'
      }
    ],
    actions: rootActions
  })
}
