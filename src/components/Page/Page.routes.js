/* x-import-routes */
import Feed from './Feed/Feed.routes'

const children = [
// eslint-disable-next-line
/* x-spread-children */
  ...Feed
]

export default () => [{
  path: '/page',
  component: () => import('./Page.vue'),
  meta: {
    requiresLogin: true
  },
  children
}]
