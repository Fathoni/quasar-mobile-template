/* x-import-store */
import Feed from './Feed/Feed.store.js'

let modules = {
// eslint-disable-next-line
/* x-add-module */
  Feed
}

const state = {}

const getters = {}

const mutations = {}

const actions = {}

const namespaced = true

export default {
  namespaced,
  state,
  getters,
  mutations,
  actions,
  modules
}
