const usedComponents = [
  'QBtn',
  'QCard',
  'QCardActions',
  'QCardMain',
  'QCardMedia',
  'QCardSeparator',
  'QCardTitle',
  'QCheckbox',
  'QCollapsible',
  'QDataTable',
  'QIcon',
  'QInput',
  'QItem',
  'QItemMain',
  'QItemSide',
  'QLayout',
  'QList',
  'QListHeader',
  'QPopover',
  'QRouteTab',
  'QScrollArea',
  'QSearch',
  'QSideLink',
  'QTabs',
  'QToolbar',
  'QToolbarTitle',
  'QTransition',
  'QUploader'
]

const dataTableConfig = {
  refresh: true,
  noHeader: false,
  columnPicker: true,
  selection: 'single',
  bodyStyle: {
    maxHeight: '500px'
  },
  rowHeight: '60px',
  responsive: true,
  pagination: {
    rowsPerPage: 10,
    options: [5, 10, 15, 30, 50, 500]
  }
}

export {
  dataTableConfig,
  usedComponents
}
